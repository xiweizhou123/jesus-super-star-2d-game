#include "mainstage.h"
#include "utils.h"
#include "input.h"
#include "image.h"
#include "framework.h"
#include "game.h"
#include <map>
#include <cmath>
#include <vector>
#include "game.h"



std::vector<int> keys = { 82, 81, 80, 79 };

using namespace std;

map<int, int> create_map()
{
	map<int, int> m;
	m[82] = 0;
	m[81] = 1;
	m[80] = 2;
	m[79] = 3;

	return m;
}

map<int, int> keys_mapping = create_map();

mainstage::mainstage() : stage()
{
	

}

mainstage::mainstage(const Image& font, const Image& minifont, const Image& sprite) : stage()
{

	stage::type = 2;
	onscreentime = 1000;
	number_of_arrows= 2;


	for (int x = 0; x < number_of_arrows; ++x) {

		random_arrows.push_back(*select_randomly(keys.begin(), keys.end()));

	}

	score = "miss";
	space_pressed = false;
	stage::hit = false;
	stage::mode = 1;
	score_number = 0;

	stg_font = font;
	stg_minifont = minifont;
	stg_sprite = sprite;

	stg_font.Set("data/bitmap-font-black.tga","b_font");
	stg_sprite.Set("data/spritesheet.tga", "jesus");
	stg_sprite.Set("data/arrow.tga", "arrow");
	stg_sprite.Set("data/bar_2.tga", "bar");
	stg_sprite.Set("data/BG.tga", "bg");
	stg_sprite.Set("data/end.tga", "end");
	stg_sprite.Set("data/jesus.tga", "jesus");
	stg_sprite.Set("data/judas.tga", "judas");


	game_bar.img = *stg_sprite.Get("bar");
	game_bar.position = Vector2(55, 80);
	game_bar.size = Vector2(64, 14);

	game_ball.img = *stg_sprite.Get("bar");
	game_ball.position = Vector2(55, 80);
	game_ball.size = Vector2(13, 14);
	game_ball.velocity = 10;

	arrow.img = *stg_sprite.Get("arrow");
	arrow.position = Vector2(115/2, 98);
	arrow.size = Vector2(18, 16);

	jesus.img = *stg_sprite.Get("jesus");
	jesus.position = Vector2(20, 40);
	jesus.size = Vector2(30, 52);


	judas.img = *stg_sprite.Get("judas");
	judas.position = Vector2(20, 30);
	judas.size = Vector2(20, 30);


}


void mainstage::setvelocity(int velocity) {

	game_ball.velocity = velocity;

}

void mainstage::render(Image * framebuffer)  {
	
	//some new useful functions
	if (stage::mode == 1)
		rendergame(framebuffer);
	else
		renderend(framebuffer);

}

void mainstage::rendergame(Image * framebuffer) {

	framebuffer->drawImage(*stg_sprite.Get("bg"), 0, 0);

	if (fake_time % 4) {

		framebuffer->drawImage(game_bar.img, game_bar.position.x, game_bar.position.y, Area(0, 0, 64, 14));


	}
	else {

		framebuffer->drawImage(game_bar.img, game_bar.position.x, game_bar.position.y, Area(0, 14, 64, 14));

	}


	framebuffer->drawImage(game_bar.img, 10, 95, Area(0, 42, 112, 22));


	for (int i = 0; i < 3; i++) {


		framebuffer->drawImage(judas.img, judas.position.x * i + 50, judas.position.y, Area(int(Game::instance->time) % 4 * judas.size.x, 0, judas.size.x, judas.size.y));

	}

	if (stage::hit) {

		if (!score.compare("Excelent!"))framebuffer->drawText(score, 50, 66, stg_font, 7, 9);
		else framebuffer->drawText(score, 70, 66, stg_font, 7, 9);


	}
	else {


		framebuffer->drawImage(game_ball.img, game_ball.position.x, game_ball.position.y, Area(0, 28, 13, 14));

	}


	if ((score.compare("miss") == 0))
		framebuffer->drawImage(jesus.img, jesus.position.x, jesus.position.y, Area(int(Game::instance->time) % 2 * jesus.size.x, 0, jesus.size.x, jesus.size.y));
	else
		framebuffer->drawImage(jesus.img, jesus.position.x, jesus.position.y, Area(int(Game::instance->time) % 4 * jesus.size.x, jesus.size.y, jesus.size.x, jesus.size.y));

	draw_arrows(framebuffer);

	draw_input_arrows(framebuffer);

	framebuffer->drawText("score:" + to_string(score_number), 0, 0, stg_font, 7, 9);


}

void mainstage::renderend(Image * framebuffer) 
{

	framebuffer->drawImage(*stg_sprite.Get("end"), 0, 0);


	framebuffer->drawImage(judas.img, 10, judas.position.y, Area(int(Game::instance->time) % 4 * judas.size.x, 0, judas.size.x, judas.size.y));

	framebuffer->drawImage(judas.img, 90, judas.position.y, Area(int(Game::instance->time) % 4 * judas.size.x, 0, judas.size.x, judas.size.y));

	framebuffer->drawText("Score :" , 40, 40, stg_font, 7, 9);
	framebuffer->drawText(to_string(score_number), 40, 55 , stg_font, 7, 9);

	if (int(fake_time) % 8) { 
		framebuffer->drawText("Press Space", 25, 80, *stg_font.Get("b_font"), 7, 9); 
		framebuffer->drawText("to play again", 17, 89, *stg_font.Get("b_font"), 7, 9);
	}
}


void mainstage::onKeyUp(SDL_KeyboardEvent event)
{
	switch (event.keysym.sym)
	{


	case SDLK_UP: 
		

		if (test_keypress(SDL_SCANCODE_UP)) {

			pressed_keys.push_back(SDL_SCANCODE_UP);
			

		}

		else {
			pressed_keys.clear();
		}

		break;
	case SDLK_DOWN: 

		if (test_keypress(SDL_SCANCODE_DOWN)) {

			pressed_keys.push_back(SDL_SCANCODE_DOWN);

		}

		else {
			pressed_keys.clear();
		}
		break;
	case SDLK_LEFT: 

		if (test_keypress(SDL_SCANCODE_LEFT)) {

			pressed_keys.push_back(SDL_SCANCODE_LEFT);

		}

		else {
			pressed_keys.clear();
		}
		break;
	case SDLK_RIGHT: 

		if (test_keypress(SDL_SCANCODE_RIGHT)) {

			pressed_keys.push_back(SDL_SCANCODE_RIGHT);

		}

		else {
			pressed_keys.clear();
		}
		break;
	}
}
 
void mainstage::update(double dt) {

	fake_time++;

	if(stage::mode==1){

	
	update_ball(dt);


	if ((score.compare(""))) {

			onscreentime -= dt*500;
		
	}


	if (Input::isKeyPressed(SDL_SCANCODE_SPACE)) //if key down
	{

		if (!stage::hit) {
			space_pressed = true;

			if (((game_ball.position.x >= 89) && (game_ball.position.x <= 94)) || ((game_ball.position.x >= 98) && (game_ball.position.x <= 105))) {


				if (compare_arrows()) {
					score = "Good!";
					score_number += 500;
					stage::hit = true;
					number_of_arrows += 1;
				}

				else { score = "miss"; }

			}

			else if ((game_ball.position.x > 94) && (game_ball.position.x < 98)) {

				if (compare_arrows()) {

					score_number += 1000;
					score = "Excelent!";
					stage::hit = true;
					number_of_arrows += 1;
				}

				else { score = "miss"; }


			}
		}

	}

	}

	else reset();

}

bool mainstage::test_keypress(int keypressed) {

	
	if (random_arrows[pressed_keys.size()] == keypressed)
	{

		Game::instance->synth.playSample("data/sound/miss.wav", 1, false);

		return true;
	}

	return false; 

}

void mainstage::draw_arrows(Image * framebuffer) {


	for (int i = 0; i < random_arrows.size(); ++i) {

		int index = random_arrows[i];

		int direction = keys_mapping[index];

		float offset = 0;

		if ((random_arrows.size() == 3) || (random_arrows.size() == 4)) offset = arrow.size.x / 4;

		framebuffer->drawImage(arrow.img, (arrow.size.x )*(i)+(100*(1.0/ random_arrows.size())) + offset, arrow.position.y, Area(arrow.size.x + 1, arrow.size.y * direction, arrow.size.x+1, arrow.size.y));

	}
	

}


void mainstage::draw_input_arrows(Image * framebuffer) {


	for (int i = 0; i < pressed_keys.size(); ++i) {

		int index = random_arrows[i];

		int direction = keys_mapping[index];

		float offset = 0;

		if ((random_arrows.size() == 3) || (random_arrows.size() == 4)) offset = arrow.size.x / 4;

		framebuffer->drawImage(arrow.img, (arrow.size.x)*(i) + (100 * (1.0 / random_arrows.size())) + offset, arrow.position.y, Area(0, arrow.size.y * direction, arrow.size.x+1 , arrow.size.y));

	}


}

bool mainstage::compare_arrows() {

	if (random_arrows.size() == pressed_keys.size()) { 
		
		return true;
	}

	return false;


}

void mainstage::update_ball(double dt) {


		if (game_ball.position.x > game_bar.position.x + 52) {

		
			if (!space_pressed) {

				score = "miss";

			}

			reset();


		};

		game_ball.position.x += dt* game_ball.velocity;


}


void mainstage::reset() {

	game_ball.position.x = game_bar.position.x;
	pressed_keys.clear();
	random_arrows.clear();
	onscreentime = 1000;
	space_pressed = false;
	stage::hit = false;


	for (int x = 0; x < number_of_arrows; ++x) {

		random_arrows.push_back(*select_randomly(keys.begin(), keys.end()));

	}

	if (number_of_arrows >= 5) { number_of_arrows = 2; }

}

int mainstage::getType()
{
	return stage::type;
}



mainstage::~mainstage()
{


}



