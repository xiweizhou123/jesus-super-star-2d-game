#include "startstage.h"
#include "game.h"


Image img(128, 128);
Image anim1(128,128);
Image anim2(128, 128);
Image control(128, 128);



startstage::startstage(const Image& font, const Image& minifont, const Image& sprite) : stage()
{
	
	anim1.loadTGA("data/jesus1.tga");
	anim2.loadTGA("data/jesus2.tga");
	control.loadTGA("data/controls_2.tga");
	myfont.loadTGA("data/bitmap-font-black.tga");
	img = anim1;
	stage::mode = 1;
	stage::type = 1;
	stage::select = Vector2(15, 90);

	
	
}

startstage::startstage() : stage()
{
}

void startstage::render(Image * framebuffer)
{
	
	framebuffer->drawImage(img,0, 0, 128, 128);

	if (stage::mode == 2) {

		framebuffer->drawText("Difficulty:", 30, 80, myfont, 7, 9); 
		framebuffer->drawText("->", stage::select.x, stage::select.y, myfont, 7, 9);
		framebuffer->drawText("atheist", 30, 90, myfont, 7, 9);
		framebuffer->drawText("believer", 30, 100, myfont, 7, 9);
		framebuffer->drawText("god", 30, 110, myfont, 7, 9);
	
	}

}

void startstage::onKeyUp(SDL_KeyboardEvent event)
{

	switch (event.keysym.sym)
	{

	case SDLK_UP:

		if (stage::select.y >= 100) {

			stage::select.y -= 10;

		}

	break;

	case SDLK_DOWN:

		if (stage::select.y <= 100) {
			stage::select.y += 10;
		}

	
	break;
	}

}

void startstage::update(double dt)
{

	fake_time++;


	if (stage::mode == 1) {

		if (fake_time % 10) img = anim1;

		else {
			img = anim2;
		}

	}

	if (stage::mode == 2) {

		img = control;
	
	}

	
}


int startstage::getType()
{
	return stage::type;
}

