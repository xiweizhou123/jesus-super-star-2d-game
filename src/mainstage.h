
#ifndef MAINSTAGE_H
#define MAINSTAGE_H

#include "includes.h"
#include "image.h"
#include "utils.h"
#include "synth.h"
#include "stage.h"
#include "framework.h"
#include  <random>
#include  <iterator>


template<typename Iter, typename RandomGenerator>
Iter select_randomly(Iter start, Iter end, RandomGenerator& g) {
	std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
	std::advance(start, dis(g));
	return start;
}

template<typename Iter>
Iter select_randomly(Iter start, Iter end) {
	static std::random_device rd;
	static std::mt19937 gen(rd());
	return select_randomly(start, end, gen);
}

class mainstage : public stage
{

	Color bgcolor = Color(100, 100, 100);
	Image stg_font, stg_minifont, stg_sprite; 
	std::vector<int> pressed_keys;
	std::vector<int> random_arrows;
	float onscreentime;
	bool space_pressed;
	bool hit;
	int number_of_arrows;
	int fake_time;
	std::string score;
	int velocity = 10;
	int score_number;


	struct sprites
	{
		Vector2 position;
		Vector2 position_sprite;
		Vector2 size;
		float velocity;
		Image img;
		Area img_area;

	} arrow , game_bar, game_ball , jesus, judas;

public:

	//constructor 
	mainstage(); 
	
	mainstage(const Image & font, const Image & minifont, const Image & sprite);

	void setvelocity(int velocity);

	//mainstage(const Image & font, const Image & minifont, const Image & sprite, int velocity);

	//methods
	void render(Image * framebuffer);
	void rendergame(Image * framebuffer);
	void renderend(Image * framebuffer);
	void onKeyUp(SDL_KeyboardEvent event);
	void update(double dt);
	bool test_keypress(int keypressed);
	void draw_arrows(Image * framebuffer);
	void draw_input_arrows(Image * framebuffer);
	bool compare_arrows();
	void update_ball(double dt);
	void reset();
	int getType();


	//destructor
	~mainstage();

	
};


#endif 