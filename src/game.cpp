#include "game.h"
#include "utils.h"
#include "input.h"
#include "image.h"
#include "mainstage.h"
#include "startstage.h"
#include <cmath>

Game* Game::instance = NULL;

Image font;
Image minifont;
Image sprite;
Color bgcolor(130, 80, 100);

Game::Game(int window_width, int window_height, SDL_Window* window)
{
	

	this->window_width = window_width;
	this->window_height = window_height;
	this->window = window;
	instance = this;
	must_exit = false;

	fps = 0;
	frame = 0;
	time = 0.0f;
	elapsed_time = 0.0f;

	font.loadTGA("data/bitmap-font-white.tga"); //load bitmap-font image
	minifont.loadTGA("data/mini-font-white-4x6.tga"); //load bitmap-font image
	sprite.loadTGA("data/spritesheet.tga"); //example to load an sprite

	gameStage = new mainstage(font, minifont, sprite);
	startStage = new startstage(font, minifont, sprite);
	currentStage = startStage;

	enableAudio(); //enable this line if you plan to add audio to your application
	//myplayback = synth.playSample("data/sound/1.wav", 1, true);
	


	myplayback = synth.playSample("data/sound/1.wav", 1, true);


	//synth.osc1.amplitude = 0.5;
}

//what to do when the image has to be draw
void Game::render(void)
{
	//Create a new Image (or we could create a global one if we want to keep the previous frame)
	Image framebuffer(128, 128);


	currentStage->render(&framebuffer);


	showFramebuffer(&framebuffer);
}

void Game::update(double seconds_elapsed)
{
	//Add here your update method
	//...
	currentStage->update(seconds_elapsed);

	if ((currentStage->getType() == 2) && (currentStage->mode == 1) &&(time >= 73.0)) {

		currentStage->mode = 2;
		std::cout << "10 secs" << std::endl;
	
	}
	
	
}

//Keyboard event handler (sync input)
void Game::onKeyDown( SDL_KeyboardEvent event )
{
	switch(event.keysym.sym)
	{
		case SDLK_ESCAPE: must_exit = true; break; //ESC key, kill the app

		case SDLK_SPACE: ; break; //ESC key, kill the app
	}
}



void Game::onKeyUp(SDL_KeyboardEvent event)
{
	currentStage->onKeyUp(event);
	
	switch (event.keysym.sym)
	{

		case SDLK_SPACE:

			

			if ((currentStage->getType() == 1) && (currentStage->mode > 1)) {

				synth.playSample("data/sound/select.wav", 1, false);

				myplayback->stop();

				myplayback= synth.playSample("data/sound/2.wav", 1.5, false);

			
				int velocity = 0; 

				if (currentStage->select.y == 90)  velocity = 10;
				
				if (currentStage->select.y == 100) velocity =  15;

				if (currentStage->select.y == 110) velocity = 25;

				gameStage->mode = 1;
				gameStage->setvelocity(velocity);

				time = 0.0;

				currentStage = gameStage;

			}

			else if ((currentStage->getType() == 1)) {

				synth.playSample("data/sound/select.wav", 1, false);
			
				currentStage->mode = 2;

			}

			else if ((currentStage->getType() == 2) && (currentStage->mode == 2)) {

				startStage->mode = 2;

				currentStage = startStage;

			}

			else if ((currentStage->getType() == 2)) {


				if(currentStage->hit)synth.playSample("data/sound/perfect.wav", 0.5, false);

				else synth.playSample("data/sound/miss.wav", 0.5, false);
				
				

			}



		break;

		case  SDLK_1:
			startStage->mode = 1;
			currentStage = startStage;
		break;

		case  SDLK_2:
			startStage->mode = 2;
			currentStage = startStage;
		break;

		case  SDLK_3:
			myplayback->stop();
			time = 0.0;
			gameStage->mode = 1;
			myplayback = synth.playSample("data/sound/2.wav", 1.5, false);
			currentStage = gameStage;
		break;

		case  SDLK_4:
			myplayback->stop();
			gameStage->mode = 2;
			currentStage = gameStage;
		break;

		
	}
}

void Game::onGamepadButtonDown(SDL_JoyButtonEvent event)
{

}

void Game::onGamepadButtonUp(SDL_JoyButtonEvent event)
{

}

void Game::onMouseMove(SDL_MouseMotionEvent event)
{
}

void Game::onMouseButtonDown( SDL_MouseButtonEvent event )
{
}

void Game::onMouseButtonUp(SDL_MouseButtonEvent event)
{
}

void Game::onMouseWheel(SDL_MouseWheelEvent event)
{
}

void Game::onResize(int width, int height)
{
    std::cout << "window resized: " << width << "," << height << std::endl;
	glViewport( 0,0, width, height );
	window_width = width;
	window_height = height;
}

//sends the image to the framebuffer of the GPU
void Game::showFramebuffer(Image* img)
{
	static Image finalframe;

	if (window_width < img->width * 4 || window_height < img->height * 4)
	{
		finalframe = *img;
		finalframe.scale( window_width, window_height );
	}
	else
	{
		if (finalframe.width != window_width || finalframe.height != window_height)
		{
			finalframe.resize(window_width, window_height);
			finalframe.fill(Color::BLACK);
		}
		finalframe.drawImage(*img, (window_width - img->width * 4) * 0.5, (window_height - img->height * 4) * 0.5, img->width * 4, img->height * 4);
	}

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	if (1) //flip
	{
		glRasterPos2f(-1, 1);
		glPixelZoom(1, -1);
	}

	glDrawPixels(finalframe.width, finalframe.height, GL_RGBA, GL_UNSIGNED_BYTE, finalframe.pixels);
}

//AUDIO STUFF ********************

SDL_AudioSpec audio_spec;

void AudioCallback(void*  userdata,
	Uint8* stream,
	int    len)
{
	static double audio_time = 0;

	memset(stream, 0, len);//clear
	if (!Game::instance)
		return;

	Game::instance->onAudio((float*)stream, len / sizeof(float), audio_time, audio_spec);
	audio_time += len / (double)audio_spec.freq;
}

void Game::enableAudio()
{
	SDL_memset(&audio_spec, 0, sizeof(audio_spec)); /* or SDL_zero(want) */
	audio_spec.freq = 48000;
	audio_spec.format = AUDIO_F32;
	audio_spec.channels = 1;
	audio_spec.samples = 1024;
	audio_spec.callback = AudioCallback; /* you wrote this function elsewhere. */
	if (SDL_OpenAudio(&audio_spec, &audio_spec) < 0) {
		fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
		exit(-1);
	}
	SDL_PauseAudio(0);
}

void Game::onAudio(float *buffer, unsigned int len, double time, SDL_AudioSpec& audio_spec)
{
	//fill the audio buffer using our custom retro synth
	synth.generateAudio(buffer, len, audio_spec);
}
