#ifndef STAGE_H
#define STAGE_H
#include "image.h" 
#include "input.h"
#include "framework.h"
#include "synth.h"

class stage
{
	

public:
	stage();
	int mode;
	int type;
	int hit;
	Vector2 select;

	virtual void render(Image * framebuffer) = 0;
	virtual void update(double dt) = 0;
	virtual int getType() = 0;
	virtual void onKeyUp(SDL_KeyboardEvent event) = 0;
	

};




#endif 