#ifndef STARTSTAGE_H
#define STARTSTAGE_H

#include "stage.h"
#include "input.h"

//Starting stage / scene. Here we just load the welcome menu of the game
class startstage : public stage
{

	Image myfont;
	Vector2 select;
public:
	startstage(const Image& font, const Image& minifont, const Image& sprite);
	startstage();
	//~StartStage();
	int fake_time;
	
	void render(Image * framebuffer);

	void onKeyUp(SDL_KeyboardEvent event);

	void update(double dt);

	int getType();

};

#endif

